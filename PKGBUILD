# Maintainer: Future Linux Team <future_linux@163.com>

pkgname=glib
pkgver=2.72.3
pkgrel=1
pkgdesc="Low level core library"
arch=('x86_64')
url="https://wiki.gnome.org/Projects/GLib"
license=('LGPL')
depends=('pcre' 'libffi' 'docbook-xsl' 'util-linux' 'zlib')
makedepends=('gettext' 'python' 'libelf' 'meson' 'dbus')
optdepends=('python: gdbus-codegen, glib-genmarshal, glib-mkenums, gtester-report'
    'libelf: gresource inspection tool'
    'gvfs: most gio functionality')
source=(https://download.gnome.org/sources/glib/${pkgver%.*}/${pkgname}-${pkgver}.tar.xz
    ${pkgname}-${pkgver}-skip_warnings-1.patch
    gio-querymodules.hook
    glib-compile-schemas.hook
    gio-querymodules.script)
sha256sums=(4a39a2f624b8512d500d5840173eda7fa85f51c109052eae806acece85d345f0
    8f9ee9f4a6a08c49c9c912241c63d55b969950c49f4d40337c6fd9557b9daa1b
    930fad09c610f3ea41abc20de9cf2e645b468c7cf0b4045b0daa94cd8e67934b
    64ae5597dda3cc160fc74be038dbe6267d41b525c0c35da9125fbf0de27f9b25
    92d08db5aa30bda276bc3d718e7ff9dd01dc40dcab45b359182dcc290054e24e)

prepare() {
    cd ${pkgname}-${pkgver}

    patch -p1 -i ${srcdir}/${pkgname}-${pkgver}-skip_warnings-1.patch
}

build() {
    cd ${pkgname}-${pkgver}

    meson --prefix=/usr     \
        --buildtype=release \
        -Dman=true          \
        build

    meson compile -C build
}

package() {
    cd ${pkgname}-${pkgver}

    meson install -C build --destdir ${pkgdir}

    install -Dt ${pkgdir}/usr/share/libalpm/hooks -m644 ${srcdir}/*.hook
    install -D ${srcdir}/gio-querymodules.script ${pkgdir}/usr/share/libalpm/scripts/gio-querymodules

    python3 -m compileall -d /usr/share/glib-2.0/codegen \
        ${pkgdir}/usr/share/glib-2.0/codegen
    python3 -O -m compileall -d /usr/share/glib-2.0/codegen \
        ${pkgdir}/usr/share/glib-2.0/codegen
}
